class State(object):
    
    def __init__(self):
        self.a = 0
        self.b = 0
        self.c = 0
        self.d = 0
        self.e = 0
        self.h = 0
        self.l = 0
        self.sp = 0
        self.pc = 0
        self.memory = [0] * 0xffff
        self.int_enable = 0
        self.z = False
        self.s = False
        self.p = False
        self.cy = False
        self.ac = False
        self.pad = False
   
    def __str__(self):
        return str({'a': '{:02x}'.format(self.a),
            'b': '{:02x}'.format(self.b),
            'c': '{:02x}'.format(self.c),
            'd': '{:02x}'.format(self.d),
            'e': '{:02x}'.format(self.e),
            'h': '{:02x}'.format(self.h),
            'l': '{:02x}'.format(self.l),
            'sp': '{:04x}'.format(self.sp),
            'pc': '{:04x}'.format(self.pc),
            'flags': [self.z, self.s, self.p, self.cy, self.ac, self.pad]})

