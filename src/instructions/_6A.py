from instructions.Instruction import Instruction

class _6A(Instruction):

    def __init__(self):
        super(_6A, self).__init__('6A', 'MOV L,D', 1, None)

    def function(self, state):
        # L <- D
        raise NotImplementedError()
