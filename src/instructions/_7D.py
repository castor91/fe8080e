from instructions.Instruction import Instruction
class _7D(Instruction):
    def __init__(self):
        super(_7D, self).__init__('7D', 'MOV A,L', 1, 'None')
    def function(self, state):
        # A <- L

        raise NotImplementedError()
