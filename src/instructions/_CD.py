from instructions.Instruction import Instruction
class _CD(Instruction):
    def __init__(self):
        super(_CD, self).__init__('CD', 'CALL', 3, 'None')
        self._value = None

    def set_value(self, value):
        self._value = int.from_bytes(value, byteorder='little')

    def function(self, state):
        # (SP-1) <- PC.hi
        # (SP-2) <- PC.lo
        # SP <- SP + 2
        # PC = adr
        
        state.memory[state.sp - 1] = Instruction.hi(state.pc) 
        state.memory[state.sp - 2] = Instruction.lo(state.pc)
        state.sp += 2
        state.pc = self._value

    def __str__(self):
        return '{} 0x{:04x}'.format(super(_CD, self).__str__(), self._value)

