from instructions.Instruction import Instruction

class _3A(Instruction):

    def __init__(self):
        super(_3A, self).__init__('3A', 'LDA', 3, None)
        self._value = None

    def set_value(self, value):
        self._value = int.from_bytes(value, byteorder='little')


    def function(self, state):
        # A <- (adr)
        raise NotImplementedError()

    def __str__(self):
        return '{} 0x{:04x}'.format(super(_3A, self).__str__(), self._value)

