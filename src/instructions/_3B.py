from instructions.Instruction import Instruction

class _3B(Instruction):

    def __init__(self):
        super(_3B, self).__init__('3B', 'DCX SP', 1, None)

    def function(self, state):
        # SP = SP - 1
        raise NotImplementedError()
