from instructions.Instruction import Instruction

class _5C(Instruction):

    def __init__(self):
        super(_5C, self).__init__('5C', 'MOV E,H', 1, None)

    def function(self, state):
        # E <- H
        raise NotImplementedError()
