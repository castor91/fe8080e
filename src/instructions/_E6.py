from instructions.Instruction import Instruction
class _E6(Instruction):
    def __init__(self):
        super(_E6, self).__init__('E6', 'ANI D8', 2, 'Z, S, P, CY, AC')
        self._value = None

    def set_value(self, value):
        self._value = int.from_bytes(value, byteorder='little')

    def function(self, state):
        # A <- A & data
        raise NotImplementedError()

    def __str__(self):
        return '{} 0x{:04x}'.format(super(_E6, self).__str__(), self._value)

