from instructions.Instruction import Instruction

class _0A(Instruction):

    def __init__(self):
        super(_0A, self).__init__('0A', 'LDAX B', 1, None)

    def function(self, state):
        # A <- (BC)
        raise NotImplementedError()

