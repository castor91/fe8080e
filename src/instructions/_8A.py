from instructions.Instruction import Instruction
class _8A(Instruction):
    def __init__(self):
        super(_8A, self).__init__('8A', 'ADC D', 1, 'Z, S, P, CY, AC')
    def function(self, state):
        # A <- A + D + CY

        raise NotImplementedError()
