from instructions.Instruction import Instruction

class _0E(Instruction):

    def __init__(self):
        super(_0E, self).__init__('0E', 'MVI C', 2, None)
        self._value = None

    def set_value(self, value):
        self._value = int.from_bytes(value, byteorder='little')

    def function(self, state):
        # C <- byte 2
        raise NotImplementedError()

    def __str__(self):
        return '{} 0x{:02x}'.format(super(_0E, self).__str__(), self._value)

