from instructions.Instruction import Instruction

class _48(Instruction):

    def __init__(self):
        super(_48, self).__init__('48', 'MOV C,B', 1, None)

    def function(self, state):
        # C <- B
        raise NotImplementedError()
