from instructions.Instruction import Instruction
class _F1(Instruction):
    def __init__(self):
        super(_F1, self).__init__('F1', 'POP PSW', 1, 'None')
    def function(self, state):
        # flags <- (sp); A <- (sp+1); sp <- sp+2

        raise NotImplementedError()
