from instructions.Instruction import Instruction
class _97(Instruction):
    def __init__(self):
        super(_97, self).__init__('97', 'SUB A', 1, 'Z, S, P, CY, AC')
    def function(self, state):
        # A <- A - A

        raise NotImplementedError()
