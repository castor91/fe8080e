from instructions.Instruction import Instruction
class _72(Instruction):
    def __init__(self):
        super(_72, self).__init__('72', 'MOV M,D', 1, 'None')
    def function(self, state):
        # (HL) <- D

        raise NotImplementedError()
