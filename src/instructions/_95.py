from instructions.Instruction import Instruction
class _95(Instruction):
    def __init__(self):
        super(_95, self).__init__('95', 'SUB L', 1, 'Z, S, P, CY, AC')
    def function(self, state):
        # A <- A - L

        raise NotImplementedError()
