from instructions.Instruction import Instruction

class _15(Instruction):

    def __init__(self):
        super(_15, self).__init__('15', 'DCR D', 1, 'Z, S, P, AC')

    def function(self, state):
        # D <- D - 1
        raise NotImplementedError()
