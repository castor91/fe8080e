from instructions.Instruction import Instruction

class _4B(Instruction):

    def __init__(self):
        super(_4B, self).__init__('4B', 'MOV C,E', 1, None)

    def function(self, state):
        # C <- E
        raise NotImplementedError()
