from instructions.Instruction import Instruction

class _22(Instruction):

    def __init__(self):
        super(_22, self).__init__('22', 'SHLD', 3, None)
        self._value = None

    def set_value(self, value):
        self._value = int.from_bytes(value, byteorder='little')

    def function(self, state):
        # (adr) <- L
        # (adr + 1) <- H
        raise NotImplementedError()
    
    def __str__(self):
        return '{} 0x{:04x}'.format(super(_22, self).__str__(), self._value)

