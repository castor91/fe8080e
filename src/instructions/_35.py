from instructions.Instruction import Instruction

class _35(Instruction):

    def __init__(self):
        super(_35, self).__init__('35', 'DCR M', 1, 'Z, S, P, AC')

    def function(self, state):
        # (HL) <- (HL - 1)
        raise NotImplementedError()
