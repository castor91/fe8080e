from instructions.Instruction import Instruction

class _0B(Instruction):

    def __init__(self):
        super(_0B, self).__init__('0B', 'DCX B', 1, None)

    def function(self, state):
        # BC = BC - 1
        raise NotImplementedError()

