from instructions.Instruction import Instruction

class _19(Instruction):

    def __init__(self):
        super(_19, self).__init__('19', 'DAD D', 1, 'CY')

    def function(self, state):
        # HL = HL + DE
        raise NotImplementedError()
