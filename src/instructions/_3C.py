from instructions.Instruction import Instruction

class _3C(Instruction):

    def __init__(self):
        super(_3C, self).__init__('3C', 'INR A', 1, 'Z, S, P, AC')

    def function(self, state):
        # A <- A + 1
        raise NotImplementedError()
