from instructions.Instruction import Instruction
class _CA(Instruction):
    def __init__(self):
        super(_CA, self).__init__('CA', 'JZ', 3, 'None')
        self._value = None

    def set_value(self, value):
        self._value = int.from_bytes(value, byteorder='little')

    def function(self, state):
        # if Z, PC <- adr
        raise NotImplementedError()

    def __str__(self):
        return '{} 0x{:04x}'.format(super(_CA, self).__str__(), self._value)

