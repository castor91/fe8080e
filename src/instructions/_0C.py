from instructions.Instruction import Instruction

class _0C(Instruction):

    def __init__(self):
        super(_0C, self).__init__('0C', 'INR C', 1, 'Z, S, P, AC')

    def function(self, state):
        # C <- C + 1
        raise NotImplementedError()

