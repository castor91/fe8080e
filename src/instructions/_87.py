from instructions.Instruction import Instruction
class _87(Instruction):
    def __init__(self):
        super(_87, self).__init__('87', 'ADD A', 1, 'Z, S, P, CY, AC')
    def function(self, state):
        # A <- A + A

        raise NotImplementedError()
