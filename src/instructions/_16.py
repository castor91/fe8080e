from instructions.Instruction import Instruction

class _16(Instruction):

    def __init__(self):
        super(_16, self).__init__('16', 'MVI D', 2, None)
        self._value = None

    def set_value(self, value):
        self._value = int.from_bytes(value, byteorder='little')

    def function(self, state):
        # D <- byte 2
        raise NotImplementedError()

    def __str__(self):
        return '{} 0x{:02x}'.format(super(_16, self).__str__(), self._value)
