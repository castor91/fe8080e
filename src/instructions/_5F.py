from instructions.Instruction import Instruction

class _5F(Instruction):

    def __init__(self):
        super(_5F, self).__init__('5F', 'MOV E,A', 1, None)

    def function(self, state):
        # E <- A
        raise NotImplementedError()
