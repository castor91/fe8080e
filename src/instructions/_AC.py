from instructions.Instruction import Instruction
class _AC(Instruction):
    def __init__(self):
        super(_AC, self).__init__('AC', 'XRA H', 1, 'Z, S, P, CY, AC')
    def function(self, state):
        # A <- A ^ H

        raise NotImplementedError()
