from instructions.Instruction import Instruction

class _6B(Instruction):

    def __init__(self):
        super(_6B, self).__init__('6B', 'MOV L,E', 1, None)

    def function(self, state):
        # L <- E
        raise NotImplementedError()
