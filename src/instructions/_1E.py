from instructions.Instruction import Instruction

class _1E(Instruction):

    def __init__(self):
        super(_1E, self).__init__('1E', 'MVI E', 2, None)
        self._value = None

    def set_value(self, value):
        self._value = int.from_bytes(value, byteorder='little')

    def function(self, state):
        # E <- byte 2
        raise NotImplementedError()

    def __str__(self):
        return '{} 0x{:04x}'.format(super(_1E, self).__str__(), self._value)
