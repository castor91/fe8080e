from instructions.Instruction import Instruction
class _BC(Instruction):
    def __init__(self):
        super(_BC, self).__init__('BC', 'CMP H', 1, 'Z, S, P, CY, AC')
    def function(self, state):
        # A - H

        raise NotImplementedError()
