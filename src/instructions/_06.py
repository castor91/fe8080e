from instructions.Instruction import Instruction

class _06(Instruction):

    def __init__(self):
        super(_06, self).__init__('06', 'MVI B', 2, None)
        self._value = None

    def set_value(self, value):
        self._value = int.from_bytes(value, byteorder='little')

    def function(self, state):
        # B <- byte 2
        state.b = self._value
        state.pc += self.get_size()
    
    def __str__(self):
        return '{} 0x{:02x}'.format(super(_06, self).__str__(), self._value)
