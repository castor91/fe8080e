from instructions.Instruction import Instruction

class _3F(Instruction):

    def __init__(self):
        super(_3F, self).__init__('3F', 'CMC', 1, 'CY')

    def function(self, state):
        # CY = !CY
        raise NotImplementedError()
