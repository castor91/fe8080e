from instructions.Instruction import Instruction

class _6C(Instruction):

    def __init__(self):
        super(_6C, self).__init__('6C', 'MOV L,H', 1, None)

    def function(self, state):
        # L <- H
        raise NotImplementedError()
