from instructions.Instruction import Instruction
class _F2(Instruction):
    def __init__(self):
        super(_F2, self).__init__('F2', 'JP', 3, 'None')
        self._value = None

    def set_value(self, value):
        self._value = int.from_bytes(value, byteorder='little')

    def function(self, state):
        # if P=1 PC <- adr
        raise NotImplementedError()

    def __str__(self):
        return '{} 0x{:04x}'.format(super(_F2, self).__str__(), self._value)

