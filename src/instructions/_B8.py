from instructions.Instruction import Instruction
class _B8(Instruction):
    def __init__(self):
        super(_B8, self).__init__('B8', 'CMP B', 1, 'Z, S, P, CY, AC')
    def function(self, state):
        # A - B

        raise NotImplementedError()
