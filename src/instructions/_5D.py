from instructions.Instruction import Instruction

class _5D(Instruction):

    def __init__(self):
        super(_5D, self).__init__('5D', 'MOV E,L', 1, None)

    def function(self, state):
        # E <- L
        raise NotImplementedError()
