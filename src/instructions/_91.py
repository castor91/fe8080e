from instructions.Instruction import Instruction
class _91(Instruction):
    def __init__(self):
        super(_91, self).__init__('91', 'SUB C', 1, 'Z, S, P, CY, AC')
    def function(self, state):
        # A <- A - C

        raise NotImplementedError()
