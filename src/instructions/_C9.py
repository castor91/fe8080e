from instructions.Instruction import Instruction
class _C9(Instruction):

    def __init__(self):
        super(_C9, self).__init__('C9', 'RET', 1, 'None')

    def function(self, state):
        # PC.lo <- (sp); PC.hi<-(sp+1); SP <- SP+2
        state.pc = state.memory[state.sp + 1] << 8 + state.memory[state.sp]

