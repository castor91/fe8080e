from instructions.Instruction import Instruction

class _61(Instruction):

    def __init__(self):
        super(_61, self).__init__('61', 'MOV H,C', 1, None)

    def function(self, state):
        # H <- C
        raise NotImplementedError()
