from instructions.Instruction import Instruction
class _99(Instruction):
    def __init__(self):
        super(_99, self).__init__('99', 'SBB C', 1, 'Z, S, P, CY, AC')
    def function(self, state):
        # A <- A - C - CY

        raise NotImplementedError()
