from instructions.Instruction import Instruction

class _27(Instruction):

    def __init__(self):
        super(_27, self).__init__('27', 'DAA', 1, None)

    def function(self, state):
        # special
        raise NotImplementedError()
