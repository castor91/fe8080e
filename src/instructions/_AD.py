from instructions.Instruction import Instruction
class _AD(Instruction):
    def __init__(self):
        super(_AD, self).__init__('AD', 'XRA L', 1, 'Z, S, P, CY, AC')
    def function(self, state):
        # A <- A ^ L

        raise NotImplementedError()
