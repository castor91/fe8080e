from instructions.Instruction import Instruction
class _FA(Instruction):
    def __init__(self):
        super(_FA, self).__init__('FA', 'JM', 3, 'None')
        self._value = None

    def set_value(self, value):
        self._value = int.from_bytes(value, byteorder='little') 

    def function(self, state):
        # if M, PC <- adr
        raise NotImplementedError()

    def __str__(self):
        return '{} 0x{:04x}'.format(super(_FA, self).__str__(), self._value)

