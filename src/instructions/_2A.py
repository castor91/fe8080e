from instructions.Instruction import Instruction

class _2A(Instruction):

    def __init__(self):
        super(_2A, self).__init__('2A', 'LHLD', 3, None)
        self._value = None

    def set_value(self, value):
        self._value = int.from_bytes(value, byteorder='little')

    def function(self, state):
        # L <- (adr)
        # H <- (adr + 1)
        raise NotImplementedError()

    def __str__(self):
        return '{} 0x{:04x}'.format(super(_2A, self).__str__(), self._value)

