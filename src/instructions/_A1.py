from instructions.Instruction import Instruction
class _A1(Instruction):
    def __init__(self):
        super(_A1, self).__init__('A1', 'ANA C', 1, 'Z, S, P, CY, AC')
    def function(self, state):
        # A <- A & C

        raise NotImplementedError()
