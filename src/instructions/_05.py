from instructions.Instruction import Instruction

class _05(Instruction):

    def __init__(self):
        super(_05, self).__init__('05', 'DCR B', 1, 'Z, S, P, AC')

    def function(self, state):
        # B <- B - 1
        state.b -= 1
        state.z = (state.b & 0xFF) == 0
        state.s = (state.b & 0x80) != 0
        state.cy = state.b > 0xff
        # TODO Parity
        state.b %= 0xFF
        state.pc += self.get_size()

