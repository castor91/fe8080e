from instructions.Instruction import Instruction
class _CE(Instruction):
    def __init__(self):
        super(_CE, self).__init__('CE', 'ACI D8', 2, 'Z, S, P, CY, AC')
    def function(self, state):
        # A <- A + data + CY

        raise NotImplementedError()
