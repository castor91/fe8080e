from instructions.Instruction import Instruction
class _A7(Instruction):
    def __init__(self):
        super(_A7, self).__init__('A7', 'ANA A', 1, 'Z, S, P, CY, AC')
    def function(self, state):
        # A <- A & A

        raise NotImplementedError()
