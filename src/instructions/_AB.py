from instructions.Instruction import Instruction
class _AB(Instruction):
    def __init__(self):
        super(_AB, self).__init__('AB', 'XRA E', 1, 'Z, S, P, CY, AC')
    def function(self, state):
        # A <- A ^ E

        raise NotImplementedError()
