from instructions.Instruction import Instruction
class _C0(Instruction):
    def __init__(self):
        super(_C0, self).__init__('C0', 'RNZ', 1, 'None')
    def function(self, state):
        # if NZ, RET

        raise NotImplementedError()
