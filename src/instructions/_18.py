from instructions.Instruction import Instruction

class _18(Instruction):

    def __init__(self):
        super(_18, self).__init__(None, None, 1, None)

    def function(self, state):
        raise NotImplementedError()
