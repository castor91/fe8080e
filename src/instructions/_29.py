from instructions.Instruction import Instruction

class _29(Instruction):

    def __init__(self):
        super(_29, self).__init__('29', 'DAD H', 1, 'CY')

    def function(self, state):
        # HL = HL + HI
        raise NotImplementedError()
