from instructions.Instruction import Instruction
class _7C(Instruction):
    def __init__(self):
        super(_7C, self).__init__('7C', 'MOV A,H', 1, 'None')
    def function(self, state):
        # A <- H

        raise NotImplementedError()
