from instructions.Instruction import Instruction

class _13(Instruction):

    def __init__(self):
        super(_13, self).__init__('13', 'INX D', 1, None)

    def function(self, state):
        # DE <- DE + 1
        value = int('{:02x}{:02x}'.format(state.d, state.e), 16)
        value = (value + 1) % 0xFF
        state.d = Instruction.hi(value)
        state.e = Instruction.lo(value)
        state.pc += self.get_size()

