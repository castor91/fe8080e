from instructions.Instruction import Instruction
class _D0(Instruction):
    def __init__(self):
        super(_D0, self).__init__('D0', 'RNC', 1, 'None')
    def function(self, state):
        # if NCY, RET

        raise NotImplementedError()
