from instructions.Instruction import Instruction

class _69(Instruction):

    def __init__(self):
        super(_69, self).__init__('69', 'MOV L,C', 1, None)

    def function(self, state):
        # L <- C
        raise NotImplementedError()
