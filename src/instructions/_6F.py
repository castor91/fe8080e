from instructions.Instruction import Instruction

class _6F(Instruction):

    def __init__(self):
        super(_6F, self).__init__('6F', 'MOV L,A', 1, None)

    def function(self, state):
        # L <- A
        raise NotImplementedError()
