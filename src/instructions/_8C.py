from instructions.Instruction import Instruction
class _8C(Instruction):
    def __init__(self):
        super(_8C, self).__init__('8C', 'ADC H', 1, 'Z, S, P, CY, AC')
    def function(self, state):
        # A <- A + H + CY

        raise NotImplementedError()
