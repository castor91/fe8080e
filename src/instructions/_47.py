from instructions.Instruction import Instruction

class _47(Instruction):

    def __init__(self):
        super(_47, self).__init__('47', 'MOV B,A', 1, None)

    def function(self, state):
        # B <- A
        raise NotImplementedError()
