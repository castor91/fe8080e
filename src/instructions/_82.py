from instructions.Instruction import Instruction
class _82(Instruction):
    def __init__(self):
        super(_82, self).__init__('82', 'ADD D', 1, 'Z, S, P, CY, AC')
    def function(self, state):
        # A <- A + D

        raise NotImplementedError()
