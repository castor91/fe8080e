from instructions.Instruction import Instruction
class _B5(Instruction):
    def __init__(self):
        super(_B5, self).__init__('B5', 'ORA L', 1, 'Z, S, P, CY, AC')
    def function(self, state):
        # A <- A | L

        raise NotImplementedError()
