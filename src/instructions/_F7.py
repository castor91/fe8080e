from instructions.Instruction import Instruction
class _F7(Instruction):
    def __init__(self):
        super(_F7, self).__init__('F7', 'RST 6', 1, 'None')
    def function(self, state):
        # CALL $30

        raise NotImplementedError()
