from instructions.Instruction import Instruction

class _28(Instruction):

    def __init__(self):
        super(_28, self).__init__(None, None, 1, None)
        
    def function(self, state):
        raise NotImplementedError()

