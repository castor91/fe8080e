from instructions.Instruction import Instruction

class _2C(Instruction):

    def __init__(self):
        super(_2C, self).__init__('2C', 'INR L', 1, 'Z, S, P, AC')

    def function(self, state):
        # L <- L + 1
        raise NotImplementedError()
