from instructions.Instruction import Instruction

class _38(Instruction):

    def __init__(self):
        super(_38, self).__init__(None, None, 1, None )
        
    def function(self, state):
        raise NotImplementedError()
