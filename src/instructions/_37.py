from instructions.Instruction import Instruction

class _37(Instruction):

    def __init__(self):
        super(_37, self).__init__('37', 'STC', 1, 'CY')

    def function(self, state):
        # CY = 1
        raise NotImplementedError()
