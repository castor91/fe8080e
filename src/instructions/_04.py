from instructions.Instruction import Instruction

class _04(Instruction):

    def __init__(self):
        super(_04, self).__init__('04', 'INR B', 1, 'Z, S, P, AC')

    def function(self, state):
        # B <- B + 1
        raise NotImplementedError()
