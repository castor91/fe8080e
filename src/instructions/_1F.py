from instructions.Instruction import Instruction

class _1F(Instruction):

    def __init__(self):
        super(_1F, self).__init__('1F', 'RAR', 1, 'CY')

    def function(self, state):
        # A = A >> 1
        # bit 7 = prev bit 7
        # CY = prev bit 0
        raise NotImplementedError()
