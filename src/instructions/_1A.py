from instructions.Instruction import Instruction

class _1A(Instruction):

    def __init__(self):
        super(_1A, self).__init__('1A', 'LDAX D', 1, None)

    def function(self, state):
        # A <- (DE)
        adr = int('{:02x}{:02x}'.format(state.d, state.e), 16)
        state.a = state.memory[adr]
        state.pc += self.get_size()

