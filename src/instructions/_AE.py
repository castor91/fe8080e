from instructions.Instruction import Instruction
class _AE(Instruction):
    def __init__(self):
        super(_AE, self).__init__('AE', 'XRA M', 1, 'Z, S, P, CY, AC')
    def function(self, state):
        # A <- A ^ (HL)

        raise NotImplementedError()
