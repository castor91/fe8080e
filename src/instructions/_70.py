from instructions.Instruction import Instruction
class _70(Instruction):
    def __init__(self):
        super(_70, self).__init__('70', 'MOV M,B', 1, 'None')
    def function(self, state):
        # (HL) <- B

        raise NotImplementedError()
