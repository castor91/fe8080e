from instructions.Instruction import Instruction

class _51(Instruction):

    def __init__(self):
        super(_51, self).__init__('51', 'MOV D,C', 1, None)

    def function(self, state):
        # D <- C
        raise NotImplementedError()
