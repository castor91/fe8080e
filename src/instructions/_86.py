from instructions.Instruction import Instruction
class _86(Instruction):
    def __init__(self):
        super(_86, self).__init__('86', 'ADD M', 1, 'Z, S, P, CY, AC')

    def function(self, state):
        # A <- A + (HL)
        result = (state.h << 8) + state.l 
        state.z = (result & 0xff) == 0    
        state.s = (result & 0x80) != 0    
        state.cy = result > 0xff    
        # TODO Parity
        state.a = result % 0xFF
        state.pc += self.get_size()

