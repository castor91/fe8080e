from instructions.Instruction import Instruction
class _E5(Instruction):
    def __init__(self):
        super(_E5, self).__init__('E5', 'PUSH H', 1, 'None')
    def function(self, state):
        # (sp-2)<-L; (sp-1)<-H; sp <- sp - 2

        raise NotImplementedError()
