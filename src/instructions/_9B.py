from instructions.Instruction import Instruction
class _9B(Instruction):
    def __init__(self):
        super(_9B, self).__init__('9B', 'SBB E', 1, 'Z, S, P, CY, AC')
    def function(self, state):
        # A <- A - E - CY

        raise NotImplementedError()
