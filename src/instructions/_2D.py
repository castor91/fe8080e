from instructions.Instruction import Instruction

class _2D(Instruction):

    def __init__(self):
        super(_2D, self).__init__('2D', 'DCR L', 1, 'Z, S, P, AC')

    def function(self, state):
        # L <- L - 1
        raise NotImplementedError()
