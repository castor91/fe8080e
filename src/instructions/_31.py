from instructions.Instruction import Instruction

class _31(Instruction):

    def __init__(self):
        super(_31, self).__init__('31', 'LXI SP', 3, None)
        self._value = None

    def set_value(self, value):
        self._value = int.from_bytes(value, byteorder='little')

    def function(self, state):
        # SP.hi <- byte 3
        # SP.lo <- byte 2
        value = int('{:02x}{:02x}'.format(Instruction.hi(self._value), Instruction.lo(self._value)), 16)
        state.sp = value
        state.pc += self.get_size()

    def __str__(self):
        return '{} 0x{:04x}'.format(super(_31, self).__str__(), self._value)

