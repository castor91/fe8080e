from instructions.Instruction import Instruction

class _11(Instruction):

    def __init__(self):
        super(_11, self).__init__('11', 'LXI D', 3, None)
        self._value = None

    def set_value(self, value):
        self._value = int.from_bytes(value, byteorder='little')

    def function(self, state):
        # D <- byte 3
        # E <- byte 2
        state.d = Instruction.hi(self._value)
        state.e = Instruction.lo(self._value)
        state.pc += self.get_size()

    def __str__(self):
        return '{} 0x{:04x}'.format(super(_11, self).__str__(), self._value)

