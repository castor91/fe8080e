from instructions.Instruction import Instruction
class _B9(Instruction):
    def __init__(self):
        super(_B9, self).__init__('B9', 'CMP C', 1, 'Z, S, P, CY, AC')
    def function(self, state):
        # A - C

        raise NotImplementedError()
