from instructions.Instruction import Instruction

class _53(Instruction):

    def __init__(self):
        super(_53, self).__init__('53', 'MOV D,E', 1, None)

    def function(self, state):
        # D <- E
        raise NotImplementedError()
