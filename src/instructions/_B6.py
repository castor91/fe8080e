from instructions.Instruction import Instruction
class _B6(Instruction):
    def __init__(self):
        super(_B6, self).__init__('B6', 'ORA M', 1, 'Z, S, P, CY, AC')
    def function(self, state):
        # A <- A | (HL)

        raise NotImplementedError()
