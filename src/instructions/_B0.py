from instructions.Instruction import Instruction
class _B0(Instruction):
    def __init__(self):
        super(_B0, self).__init__('B0', 'ORA B', 1, 'Z, S, P, CY, AC')
    def function(self, state):
        # A <- A | B

        raise NotImplementedError()
