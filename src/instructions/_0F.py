from instructions.Instruction import Instruction

class _0F(Instruction):

    def __init__(self):
        super(_0F, self).__init__('0F', 'RRC', 1, 'CY')

    def function(self, state):
        # A = A >> 1
        # bit 7 = prev bit 0
        # CY = prev bit 0
        raise NotImplementedError()
