from instructions.Instruction import Instruction

class _21(Instruction):

    def __init__(self):
        super(_21, self).__init__('21', 'LXI H', 3, None)
        self._value = None

    def set_value(self, value):
        self._value = int.from_bytes(value, byteorder='little')

    def function(self, state):
        # H <- byte 3
        # L <- byte 2
        state.h = Instruction.hi(self._value)
        state.l = Instruction.lo(self._value)
        state.pc += self.get_size()

    def __str__(self):
        return '{} 0x{:04x}'.format(super(_21, self).__str__(), self._value)

