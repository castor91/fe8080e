from instructions.Instruction import Instruction

class _6D(Instruction):

    def __init__(self):
        super(_6D, self).__init__('6D', 'MOV L,L', 1, None)

    def function(self, state):
        # L <- L
        raise NotImplementedError()
