from instructions.Instruction import Instruction
class _BE(Instruction):
    def __init__(self):
        super(_BE, self).__init__('BE', 'CMP M', 1, 'Z, S, P, CY, AC')
    def function(self, state):
        # A - (HL)

        raise NotImplementedError()
