from instructions.Instruction import Instruction
class _80(Instruction):
    def __init__(self):
        super(_80, self).__init__('80', 'ADD B', 1, 'Z, S, P, CY, AC')

    def function(self, state):
        # A <- A + B
        state.a = state.a + state.b
        state.z = state.a & 0xFF == 0
        state.s = state.a & 0x80 != 0
        state.cy = state.a > 0xFF
        # TODO PARITY
        state.a %= 0xFF
        state.pc += self.get_size()

