from instructions.Instruction import Instruction
class _B3(Instruction):
    def __init__(self):
        super(_B3, self).__init__('B3', 'ORA E', 1, 'Z, S, P, CY, AC')
    def function(self, state):
        # A <- A | E

        raise NotImplementedError()
