from instructions.Instruction import Instruction
class _F3(Instruction):
    def __init__(self):
        super(_F3, self).__init__('F3', 'DI', 1, 'None')
    def function(self, state):
        # special

        raise NotImplementedError()
