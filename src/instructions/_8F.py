from instructions.Instruction import Instruction
class _8F(Instruction):
    def __init__(self):
        super(_8F, self).__init__('8F', 'ADC A', 1, 'Z, S, P, CY, AC')
    def function(self, state):
        # A <- A + A + CY

        raise NotImplementedError()
