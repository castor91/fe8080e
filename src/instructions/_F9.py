from instructions.Instruction import Instruction
class _F9(Instruction):
    def __init__(self):
        super(_F9, self).__init__('F9', 'SPHL', 1, 'None')
    def function(self, state):
        # SP=HL

        raise NotImplementedError()
