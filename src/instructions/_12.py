from instructions.Instruction import Instruction

class _12(Instruction):

    def __init__(self):
        super(_12, self).__init__('12', 'STAX D', 1, None)

    def function(self, state):
        # (DE) <- A
        raise NotImplementedError()

