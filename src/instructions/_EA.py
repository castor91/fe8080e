from instructions.Instruction import Instruction
class _EA(Instruction):
    def __init__(self):
        super(_EA, self).__init__('EA', 'JPE', 3, 'None')
        self._value = None

    def set_value(self, value):
        self._value = int.from_bytes(value, byteorder='little')

    def function(self, state):
        # if PE, PC <- adr
        raise NotImplementedError()

    def __str__(self):
        return '{} 0x{:04x}'.format(super(_EA, self).__str__(), self._value)
