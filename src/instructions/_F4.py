from instructions.Instruction import Instruction
class _F4(Instruction):
    def __init__(self):
        super(_F4, self).__init__('F4', 'CP', 3, 'None')
        self._value = None
    
    def set_value(self, value):
        self._value = int.from_bytes(value, byteorder='little')

    def function(self, state):
        # if P, PC <- adr
        raise NotImplementedError()

    def __str__(self):
        return '{} 0x{:04x}'.format(super(_F4, self).__str__(), self._value)
