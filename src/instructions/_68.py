from instructions.Instruction import Instruction

class _68(Instruction):

    def __init__(self):
        super(_68, self).__init__('68', 'MOV L,B', 1, None)

    def function(self, state):
        # L <- B
        raise NotImplementedError()
