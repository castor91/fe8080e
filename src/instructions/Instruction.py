class Instruction(object):

    def __init__(self, opcode, instruction, size, flags=None):
        self._opcode = opcode
        self._instruction = instruction
        self._size = int(size) if size else 0
        self._flags = flags

    def get_opcode(self):
        return self._opcode

    def get_instruction(self):
        return self._instruction

    def get_size(self):
        return self._size

    def __str__(self):
        return '{}'.format(self._instruction)

    @staticmethod
    def hi(value):
        return (value & 0xff00) >> 8
     
    @staticmethod
    def lo(value):
        return value & 0xff

