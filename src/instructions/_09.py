from instructions.Instruction import Instruction

class _09(Instruction):

    def __init__(self):
        super(_09, self).__init__('09', 'DAD B', 1, 'CY')

    def function(self, state):
        # HL = HL + BC
        raise NotImplementedError()

