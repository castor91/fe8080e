from instructions.Instruction import Instruction
class _FE(Instruction):
    def __init__(self):
        super(_FE, self).__init__('FE', 'CPI D8', 2, 'Z, S, P, CY, AC')
        self._value = None

    def set_value(self, value):
        self._value = int.from_bytes(value, byteorder='little')

    def function(self, state):
        # A - data
        raise NotImplementedError()

    def __str__(self):
        return '{} 0x{:02x}'.format(super(_FE, self).__str__(), self._value)
