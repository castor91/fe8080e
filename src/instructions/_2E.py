from instructions.Instruction import Instruction

class _2E(Instruction):

    def __init__(self):
        super(_2E, self).__init__('2E', 'MVI L', 2, None)
        self._value = None

    def set_value(self, value):
        self._value = int.from_bytes(value, byteorder='little')

    def function(self, state):
        # L <- byte 2
        raise NotImplementedError()

    def __str__(self):
        return '{} 0x{:02x}'.format(super(_2E, self).__str__(), self._value)

