from instructions.Instruction import Instruction
class _C7(Instruction):
    def __init__(self):
        super(_C7, self).__init__('C7', 'RST 0', 1, 'None')
    def function(self, state):
        # CALL $0

        raise NotImplementedError()
