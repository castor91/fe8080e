from instructions.Instruction import Instruction
class _9A(Instruction):
    def __init__(self):
        super(_9A, self).__init__('9A', 'SBB D', 1, 'Z, S, P, CY, AC')
    def function(self, state):
        # A <- A - D - CY

        raise NotImplementedError()
