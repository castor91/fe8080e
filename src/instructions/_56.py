from instructions.Instruction import Instruction

class _56(Instruction):

    def __init__(self):
        super(_56, self).__init__('56', 'MOV D,M', 1, None)

    def function(self, state):
        # D <- (HL)
        raise NotImplementedError()
