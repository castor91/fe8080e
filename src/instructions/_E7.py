from instructions.Instruction import Instruction
class _E7(Instruction):
    def __init__(self):
        super(_E7, self).__init__('E7', 'RST 4', 1, 'None')
    def function(self, state):
        # CALL $20

        raise NotImplementedError()
