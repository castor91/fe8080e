from instructions.Instruction import Instruction
class _C4(Instruction):
    def __init__(self):
        super(_C4, self).__init__('C4', 'CNZ', 3, 'None')
        self._value = None

    def set_value(self, value):
        self._value = int.from_bytes(value, byteorder='little')

    def function(self, state):
        # if NZ, CALL adr
        raise NotImplementedError()

    def __str__(self):
        return '{} 0x{:04x}'.format(super(_C4, self).__str__(), self._value)

