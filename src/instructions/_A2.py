from instructions.Instruction import Instruction
class _A2(Instruction):
    def __init__(self):
        super(_A2, self).__init__('A2', 'ANA D', 1, 'Z, S, P, CY, AC')
    def function(self, state):
        # A <- A & D

        raise NotImplementedError()
