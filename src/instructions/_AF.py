from instructions.Instruction import Instruction
class _AF(Instruction):
    def __init__(self):
        super(_AF, self).__init__('AF', 'XRA A', 1, 'Z, S, P, CY, AC')
    def function(self, state):
        # A <- A ^ A

        raise NotImplementedError()
