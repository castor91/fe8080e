from instructions.Instruction import Instruction

class _4E(Instruction):

    def __init__(self):
        super(_4E, self).__init__('4E', 'MOV C,M', 1, None)

    def function(self, state):
        # C <- (HL)
        raise NotImplementedError()
