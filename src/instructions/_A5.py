from instructions.Instruction import Instruction
class _A5(Instruction):
    def __init__(self):
        super(_A5, self).__init__('A5', 'ANA L', 1, 'Z, S, P, CY, AC')
    def function(self, state):
        # A <- A & L

        raise NotImplementedError()
