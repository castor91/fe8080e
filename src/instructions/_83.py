from instructions.Instruction import Instruction
class _83(Instruction):
    def __init__(self):
        super(_83, self).__init__('83', 'ADD E', 1, 'Z, S, P, CY, AC')
    def function(self, state):
        # A <- A + E

        raise NotImplementedError()
