from instructions.Instruction import Instruction

class _07(Instruction):

    def __init__(self):
        super(_07, self).__init__('07', 'RCL', 1, 'CY')

    def function(self, state):
        # A = A << 1
        # bit 0 = prev bit 7
        # CY = prev bit 7
        raise NotImplementedError()

