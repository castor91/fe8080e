from instructions.Instruction import Instruction

class _45(Instruction):

    def __init__(self):
        super(_45, self).__init__('45', 'MOV B,L', 1, None)

    def function(self, state):
        # B <- L
        raise NotImplementedError()
