from instructions.Instruction import Instruction
class _EC(Instruction):
    def __init__(self):
        super(_EC, self).__init__('EC', 'CPE', 3, 'None')
        self._value = None

    def set_value(self, value):
        self._value = int.from_bytes(value, byteorder='little')

    def function(self, state):
        # if PE, CALL adr
        raise NotImplementedError()

    def __str__(self):
        return '{} 0x{:04x}'.format(super(_EC, self).__str__(), self._value)
