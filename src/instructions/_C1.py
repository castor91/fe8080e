from instructions.Instruction import Instruction
class _C1(Instruction):
    def __init__(self):
        super(_C1, self).__init__('C1', 'POP B', 1, 'None')
    def function(self, state):
        # C <- (sp); B <- (sp+1); sp <- sp+2

        raise NotImplementedError()
