from instructions.Instruction import Instruction
class _C8(Instruction):
    def __init__(self):
        super(_C8, self).__init__('C8', 'RZ', 1, 'None')
    def function(self, state):
        # if Z, RET

        raise NotImplementedError()
