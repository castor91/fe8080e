from instructions.Instruction import Instruction

class _14(Instruction):

    def __init__(self):
        super(_14, self).__init__('14', 'INR D', 1, 'Z, S, P, AC')

    def function(self, state):
        # D <- D + 1
        raise NotImplementedError()
