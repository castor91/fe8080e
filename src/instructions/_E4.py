from instructions.Instruction import Instruction
class _E4(Instruction):
    def __init__(self):
        super(_E4, self).__init__('E4', 'CPO', 3, 'None')
        self._value = None

    def set_value(self, value):
        self._value = int.from_bytes(value, byteorder='little')

    def function(self, state):
        # if PO, CALL adr
        raise NotImplementedError()

    def __str__(self):
        return '{} 0x{:04x}'.format(super(_E4, self).__str__(), self._value)

