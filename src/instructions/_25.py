from instructions.Instruction import Instruction

class _25(Instruction):

    def __init__(self):
        super(_25, self).__init__('25', 'DCR H', 1, 'Z, S, P, AC')

    def function(self, state):
        # H <- H - 1
        raise NotImplementedError()
