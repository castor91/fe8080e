from instructions.Instruction import Instruction

class _5B(Instruction):

    def __init__(self):
        super(_5B, self).__init__('5B', 'MOV E,E', 1, None)

    def function(self, state):
        # E <- E
        raise NotImplementedError()
