from instructions.Instruction import Instruction
class _9E(Instruction):
    def __init__(self):
        super(_9E, self).__init__('9E', 'SBB M', 1, 'Z, S, P, CY, AC')
    def function(self, state):
        # A <- A - (HL) - CY

        raise NotImplementedError()
