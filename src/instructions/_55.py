from instructions.Instruction import Instruction

class _55(Instruction):

    def __init__(self):
        super(_55, self).__init__('55', 'MOV D,L', 1, None)

    def function(self, state):
        # D <- L
        raise NotImplementedError()
