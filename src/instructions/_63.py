from instructions.Instruction import Instruction

class _63(Instruction):

    def __init__(self):
        super(_63, self).__init__('63', 'MOV H,E', 1, None)

    def function(self, state):
        # H <- E
        raise NotImplementedError()
