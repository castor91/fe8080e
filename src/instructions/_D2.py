from instructions.Instruction import Instruction
class _D2(Instruction):
    def __init__(self):
        super(_D2, self).__init__('D2', 'JNC', 3, 'None')
        self._value = None

    def set_value(self, value):
        self._value = int.from_bytes(value, byteorder='little')

    def function(self, state):
        # if NCY, PC<-adr
        raise NotImplementedError()

    def __str__(self):
        return '{} 0x{:04x}'.format(super(_D2, self).__str__(), self._value)

