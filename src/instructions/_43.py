from instructions.Instruction import Instruction

class _43(Instruction):

    def __init__(self):
        super(_43, self).__init__('43', 'MOV B,E', 1, None)

    def function(self, state):
        # B <- E
        raise NotImplementedError()
