from instructions.Instruction import Instruction

class _58(Instruction):

    def __init__(self):
        super(_58, self).__init__('58', 'MOV E,B', 1, None)

    def function(self, state):
        # E <- B
        raise NotImplementedError()
