from instructions.Instruction import Instruction

class _2B(Instruction):

    def __init__(self):
        super(_2B, self).__init__('2B', 'DCX H', 1, None)

    def function(self, state):
        # HL = HL - 1
        raise NotImplementedError()
