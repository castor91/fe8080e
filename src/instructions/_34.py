from instructions.Instruction import Instruction

class _34(Instruction):

    def __init__(self):
        super(_34, self).__init__('34', 'INR M', 1, 'Z, S, P, AC')

    def function(self, state):
        # (HL) <- (HL) + 1
        raise NotImplementedError()
