from instructions.Instruction import Instruction
class _81(Instruction):
    def __init__(self):
        super(_81, self).__init__('81', 'ADD C', 1, 'Z, S, P, CY, AC')
    def function(self, state):
        # A <- A + C

        raise NotImplementedError()
