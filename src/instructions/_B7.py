from instructions.Instruction import Instruction
class _B7(Instruction):
    def __init__(self):
        super(_B7, self).__init__('B7', 'ORA A', 1, 'Z, S, P, CY, AC')
    def function(self, state):
        # A <- A | A

        raise NotImplementedError()
