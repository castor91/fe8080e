from instructions.Instruction import Instruction

class _32(Instruction):

    def __init__(self):
        super(_32, self).__init__('32', 'STA', 3, None)
        self._value = None

    def set_value(self, value):
        self._value = int.from_bytes(value, byteorder='little')

    def function(self, state):
        # (adr) <- A
        raise NotImplementedError()

    def __str__(self):
        return '{} 0x{:04x}'.format(super(_32, self).__str__(), self._value)

