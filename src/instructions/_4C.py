from instructions.Instruction import Instruction

class _4C(Instruction):

    def __init__(self):
        super(_4C, self).__init__('4C', 'MOV C,H', 1, None)

    def function(self, state):
        # C <- H
        raise NotImplementedError()
