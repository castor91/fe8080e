from instructions.Instruction import Instruction

class _01(Instruction):

    def __init__(self):
        super(_01, self).__init__('01', 'LXI B', 3, None)
        self._value = None
    
    def set_value(self, value):
        self._value = int.from_bytes(value, byteorder='little')

    def function(self, state):
        # B <- byte 3
        # C <- byte 2
        raise NotImplementedError()

    def __str__(self):
        return '{} 0x{:04x}'.format(super(_01, self).__str__(), self._value)

