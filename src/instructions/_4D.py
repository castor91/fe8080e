from instructions.Instruction import Instruction

class _4D(Instruction):

    def __init__(self):
        super(_4D, self).__init__('4D', 'MOV C,L', 1, None)

    def function(self, state):
        # C <- L
        raise NotImplementedError()
