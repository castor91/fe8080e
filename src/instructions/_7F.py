from instructions.Instruction import Instruction
class _7F(Instruction):
    def __init__(self):
        super(_7F, self).__init__('7F', 'MOV A,A', 1, 'None')
    def function(self, state):
        # A <- A

        raise NotImplementedError()
