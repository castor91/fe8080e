from instructions.Instruction import Instruction

class _40(Instruction):

    def __init__(self):
        super(_40, self).__init__('40', 'MOV B,B', 1, None)

    def function(self, state):
        # B <- B
        raise NotImplementedError()
