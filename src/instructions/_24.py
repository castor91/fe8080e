from instructions.Instruction import Instruction

class _24(Instruction):

    def __init__(self):
        super(_24, self).__init__('24', 'INR H', 1, 'Z, S, P, AC')

    def function(self, state):
        # H <- H + 1
        raise NotImplementedError()
