from instructions.Instruction import Instruction

class _00(Instruction):

    def __init__(self):
        super(_00, self).__init__('00', 'NOP', 1, None)

    def function(self, state):
        state.pc += self.get_size()
    
