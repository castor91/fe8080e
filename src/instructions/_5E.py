from instructions.Instruction import Instruction

class _5E(Instruction):

    def __init__(self):
        super(_5E, self).__init__('5E', 'MOV E,M', 1, None)

    def function(self, state):
        # E <- (HL)
        raise NotImplementedError()
