from instructions.Instruction import Instruction

class _0D(Instruction):

    def __init__(self):
        super(_0D, self).__init__('0D', 'DCR C', 1, 'Z, S, P, AC')

    def function(self, state):
        # C <- C - 1
        raise NotImplementedError()

