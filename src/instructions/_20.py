from instructions.Instruction import Instruction

class _20(Instruction):

    def __init__(self):
        super(_20, self).__init__('20', 'RIM', 1, None)

    def function(self, state):
        # special
        raise NotImplementedError()
