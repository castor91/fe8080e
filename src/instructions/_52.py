from instructions.Instruction import Instruction

class _52(Instruction):

    def __init__(self):
        super(_52, self).__init__('52', 'MOV D,D', 1, None)

    def function(self, state):
        # D <- D
        raise NotImplementedError()
