from instructions.Instruction import Instruction
class _EB(Instruction):
    def __init__(self):
        super(_EB, self).__init__('EB', 'XCHG', 1, 'None')
    def function(self, state):
        # H <-> D; L <-> E

        raise NotImplementedError()
