from instructions.Instruction import Instruction
class _C6(Instruction):
    def __init__(self):
        super(_C6, self).__init__('C6', 'ADI D8', 2, 'Z, S, P, CY, AC')
        self._value = None

    def set_value(self, value):
        self._value = int.from_bytes(value, byteorder='little')

    def function(self, state):
        # A <- A + byte
        state.a = state.a + self._value
        state.z = (state.a & 0xFF) == 0
        state.s = (state.a & 0x80) != 0
        state.cy = (state.a > 0xFF)
        # TODO Parity
        state.a %= 0xFF
        state.pc += self.get_size()

    def __str__(self):
        return '{} 0x{:04x}'.format(super(_C6, self).__str__(), self._value)
