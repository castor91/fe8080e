from instructions.Instruction import Instruction
class _D5(Instruction):
    def __init__(self):
        super(_D5, self).__init__('D5', 'PUSH D', 1, 'None')
    def function(self, state):
        # (sp-2)<-E; (sp-1)<-D; sp <- sp - 2

        raise NotImplementedError()
