from instructions.Instruction import Instruction

class _39(Instruction):

    def __init__(self):
        super(_39, self).__init__('39', 'DAD SP', 1, 'CY')

    def function(self, state):
        # HL = HL + SP
        raise NotImplementedError()
