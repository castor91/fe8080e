from instructions.Instruction import Instruction

class _60(Instruction):

    def __init__(self):
        super(_60, self).__init__('60', 'MOV H,B', 1, None)

    def function(self, state):
        # H <- B
        raise NotImplementedError()
