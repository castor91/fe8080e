from instructions.Instruction import Instruction

class _65(Instruction):

    def __init__(self):
        super(_65, self).__init__('65', 'MOV H,L', 1, None)

    def function(self, state):
        # H <- L
        raise NotImplementedError()
