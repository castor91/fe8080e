from instructions.Instruction import Instruction
class _D1(Instruction):
    def __init__(self):
        super(_D1, self).__init__('D1', 'POP D', 1, 'None')
    def function(self, state):
        # E <- (sp); D <- (sp+1); sp <- sp+2

        raise NotImplementedError()
