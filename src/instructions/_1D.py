from instructions.Instruction import Instruction

class _1D(Instruction):

    def __init__(self):
        super(_1D, self).__init__('1D', 'DCR E', 1, 'Z, S, P, AC')

    def function(self, state):
        # E <- E - 1
        raise NotImplementedError()
