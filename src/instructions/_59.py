from instructions.Instruction import Instruction

class _59(Instruction):

    def __init__(self):
        super(_59, self).__init__('59', 'MOV E,C', 1, None)

    def function(self, state):
        # E <- C
        raise NotImplementedError()
