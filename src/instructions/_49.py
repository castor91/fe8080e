from instructions.Instruction import Instruction

class _49(Instruction):

    def __init__(self):
        super(_49, self).__init__('49', 'MOV C,C', 1, None)

    def function(self, state):
        # C <- C
        raise NotImplementedError()
