from instructions.Instruction import Instruction
class _E9(Instruction):
    def __init__(self):
        super(_E9, self).__init__('E9', 'PCHL', 1, 'None')
    def function(self, state):
        # PC.hi <- H; PC.lo <- L

        raise NotImplementedError()
