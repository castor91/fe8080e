from instructions.Instruction import Instruction

class _67(Instruction):

    def __init__(self):
        super(_67, self).__init__('67', 'MOV H,A', 1, None)

    def function(self, state):
        # H <- A
        raise NotImplementedError()
