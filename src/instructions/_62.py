from instructions.Instruction import Instruction

class _62(Instruction):

    def __init__(self):
        super(_62, self).__init__('62', 'MOV H,D', 1, None)

    def function(self, state):
        # H <- D
        raise NotImplementedError()
