from instructions.Instruction import Instruction

class _64(Instruction):

    def __init__(self):
        super(_64, self).__init__('64', 'MOV H,H', 1, None)

    def function(self, state):
        # H <- H
        raise NotImplementedError()
