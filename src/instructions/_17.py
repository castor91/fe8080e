from instructions.Instruction import Instruction

class _17(Instruction):

    def __init__(self):
        super(_17, self).__init__('17', 'RAL', 1, 'CY')

    def function(self, state):
        # A = A << 1
        # bit 0 = prev CY
        # CY = prev bit 7
        raise NotImplementedError()
