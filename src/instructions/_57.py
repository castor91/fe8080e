from instructions.Instruction import Instruction

class _57(Instruction):

    def __init__(self):
        super(_57, self).__init__('57', 'MOV D,A', 1, None)

    def function(self, state):
        # D <- A
        raise NotImplementedError()
