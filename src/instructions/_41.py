from instructions.Instruction import Instruction

class _41(Instruction):

    def __init__(self):
        super(_41, self).__init__('41', 'MOV B,C', 1, None)

    def function(self, state):
        # B <- C
        raise NotImplementedError()
