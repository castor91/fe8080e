from instructions.Instruction import Instruction

class _44(Instruction):

    def __init__(self):
        super(_44, self).__init__('44', 'MOV B,H', 1, None)

    def function(self, state):
        # B <- H
        raise NotImplementedError()
