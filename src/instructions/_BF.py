from instructions.Instruction import Instruction
class _BF(Instruction):
    def __init__(self):
        super(_BF, self).__init__('BF', 'CMP A', 1, 'Z, S, P, CY, AC')
    def function(self, state):
        # A - A

        raise NotImplementedError()
