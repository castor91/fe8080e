from instructions.Instruction import Instruction
class _D3(Instruction):
    def __init__(self):
        super(_D3, self).__init__('D3', 'OUT D8', 2, 'None')
        self._value = None

    def set_value(self, value):
        self._value = int.from_bytes(value, byteorder='little')

    def function(self, state):
        # special
        raise NotImplementedError()

    def __str__(self):
        return '{} 0x{:02x}'.format(super(_D3, self).__str__(), self._value)

