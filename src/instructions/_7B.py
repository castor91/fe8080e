from instructions.Instruction import Instruction
class _7B(Instruction):
    def __init__(self):
        super(_7B, self).__init__('7B', 'MOV A,E', 1, 'None')
    def function(self, state):
        # A <- E

        raise NotImplementedError()
