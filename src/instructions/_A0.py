from instructions.Instruction import Instruction
class _A0(Instruction):
    def __init__(self):
        super(_A0, self).__init__('A0', 'ANA B', 1, 'Z, S, P, CY, AC')
    def function(self, state):
        # A <- A & B

        raise NotImplementedError()
