from instructions.Instruction import Instruction
class _78(Instruction):
    def __init__(self):
        super(_78, self).__init__('78', 'MOV A,B', 1, 'None')
    def function(self, state):
        # A <- B

        raise NotImplementedError()
