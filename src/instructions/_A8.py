from instructions.Instruction import Instruction
class _A8(Instruction):
    def __init__(self):
        super(_A8, self).__init__('A8', 'XRA B', 1, 'Z, S, P, CY, AC')
    def function(self, state):
        # A <- A ^ B

        raise NotImplementedError()
