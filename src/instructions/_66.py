from instructions.Instruction import Instruction

class _66(Instruction):

    def __init__(self):
        super(_66, self).__init__('66', 'MOV H,M', 1, None)

    def function(self, state):
        # H <- (HL)
        raise NotImplementedError()
