from instructions.Instruction import Instruction
class _84(Instruction):
    def __init__(self):
        super(_84, self).__init__('84', 'ADD H', 1, 'Z, S, P, CY, AC')
    def function(self, state):
        # A <- A + H

        raise NotImplementedError()
