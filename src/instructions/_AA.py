from instructions.Instruction import Instruction
class _AA(Instruction):
    def __init__(self):
        super(_AA, self).__init__('AA', 'XRA D', 1, 'Z, S, P, CY, AC')
    def function(self, state):
        # A <- A ^ D

        raise NotImplementedError()
