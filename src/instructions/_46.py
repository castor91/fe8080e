from instructions.Instruction import Instruction

class _46(Instruction):

    def __init__(self):
        super(_46, self).__init__('46', 'MOV B,M', 1, None)

    def function(self, state):
        # B <- (HL)
        raise NotImplementedError()
