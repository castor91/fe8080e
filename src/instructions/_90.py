from instructions.Instruction import Instruction
class _90(Instruction):
    def __init__(self):
        super(_90, self).__init__('90', 'SUB B', 1, 'Z, S, P, CY, AC')
    def function(self, state):
        # A <- A - B

        raise NotImplementedError()
