from instructions.Instruction import Instruction

class _42(Instruction):

    def __init__(self):
        super(_42, self).__init__('42', 'MOV B,D', 1, None)

    def function(self, state):
        # B <- D
        raise NotImplementedError()
