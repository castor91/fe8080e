from instructions.Instruction import Instruction
class _C2(Instruction):
    def __init__(self):
        super(_C2, self).__init__('C2', 'JNZ', 3, 'None')
        self._value = None

    def set_value(self, value):
        self._value = int.from_bytes(value, byteorder='little')

    def function(self, state):
        # if NZ, PC <- adr
        if state.z:
            state.pc = self._value
        else:
            state.pc += self.get_size()

    def __str__(self):
        return '{} 0x{:04x}'.format(super(_C2, self).__str__(), self._value)

