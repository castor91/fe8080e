from instructions.Instruction import Instruction
class _85(Instruction):
    def __init__(self):
        super(_85, self).__init__('85', 'ADD L', 1, 'Z, S, P, CY, AC')
    def function(self, state):
        # A <- A + L

        raise NotImplementedError()
