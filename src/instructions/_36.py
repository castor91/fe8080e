from instructions.Instruction import Instruction

class _36(Instruction):

    def __init__(self):
        super(_36, self).__init__('36', 'MVI M', 2, None)
        self._value = None

    def set_value(self, value):
        self._value = int.from_bytes(value, byteorder='little')

    def function(self, state):
        # (HL) <- byte 2
        raise NotImplementedError()

    def __str__(self):
        return '{} 0x{:02x}'.format(super(_36, self).__str__(), self._value)
