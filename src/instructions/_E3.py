from instructions.Instruction import Instruction
class _E3(Instruction):
    def __init__(self):
        super(_E3, self).__init__('E3', 'XTHL', 1, 'None')
    def function(self, state):
        # L <-> (SP); H <-> (SP+1)

        raise NotImplementedError()
