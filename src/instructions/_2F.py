from instructions.Instruction import Instruction

class _2F(Instruction):

    def __init__(self):
        super(_2F, self).__init__('2F', 'CMA', 1, None)

    def function(self, state):
        # A <- !A
        raise NotImplementedError()
