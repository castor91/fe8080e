from instructions.Instruction import Instruction
class _DC(Instruction):
    def __init__(self):
        super(_DC, self).__init__('DC', 'CC', 3, 'None')
        self._value = None

    def set_value(self, value):
        self._value = int.from_bytes(value, byteorder='little')

    def function(self, state):
        # if CY, CALL adr
        raise NotImplementedError()

    def __str__(self):
        return '{} 0x{:04x}'.format(super(_DC, self).__str__(), self._value)

