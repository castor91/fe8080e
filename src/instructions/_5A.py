from instructions.Instruction import Instruction

class _5A(Instruction):

    def __init__(self):
        super(_5A, self).__init__('5A', 'MOV E,D', 1, None)

    def function(self, state):
        # E <- D
        raise NotImplementedError()
