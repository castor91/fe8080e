from instructions.Instruction import Instruction

class _54(Instruction):

    def __init__(self):
        super(_54, self).__init__('54', 'MOV D,H', 1, None)

    def function(self, state):
        # D <- H
        raise NotImplementedError()
