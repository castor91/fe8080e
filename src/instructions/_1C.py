from instructions.Instruction import Instruction

class _1C(Instruction):

    def __init__(self):
        super(_1C, self).__init__('1C', 'INR E', 1, 'Z, S, P, AC')

    def function(self, state):
        # E <- E + 1
        raise NotImplementedError()
