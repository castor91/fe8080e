from instructions.Instruction import Instruction

class _08(Instruction):

    def __init__(self):
        super(_08, self).__init__(None, None, 1, None)
    
    def function(self, state):
        raise NotImplementedError()
