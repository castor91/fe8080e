from instructions.Instruction import Instruction
class _BD(Instruction):
    def __init__(self):
        super(_BD, self).__init__('BD', 'CMP L', 1, 'Z, S, P, CY, AC')
    def function(self, state):
        # A - L

        raise NotImplementedError()
