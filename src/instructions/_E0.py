from instructions.Instruction import Instruction
class _E0(Instruction):
    def __init__(self):
        super(_E0, self).__init__('E0', 'RPO', 1, 'None')
    def function(self, state):
        # if PO, RET

        raise NotImplementedError()
