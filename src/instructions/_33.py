from instructions.Instruction import Instruction

class _33(Instruction):

    def __init__(self):
        super(_33, self).__init__('33', 'INX SP', 1, None)

    def function(self, state):
        # SP = SP + 1
        raise NotImplementedError()
