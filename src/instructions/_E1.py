from instructions.Instruction import Instruction
class _E1(Instruction):
    def __init__(self):
        super(_E1, self).__init__('E1', 'POP H', 1, 'None')
    def function(self, state):
        # L <- (sp); H <- (sp+1); sp <- sp+2

        raise NotImplementedError()
