from instructions.Instruction import Instruction
class _B2(Instruction):
    def __init__(self):
        super(_B2, self).__init__('B2', 'ORA D', 1, 'Z, S, P, CY, AC')
    def function(self, state):
        # A <- A | D

        raise NotImplementedError()
