from instructions.Instruction import Instruction
class _A3(Instruction):
    def __init__(self):
        super(_A3, self).__init__('A3', 'ANA E', 1, 'Z, S, P, CY, AC')
    def function(self, state):
        # A <- A & E

        raise NotImplementedError()
