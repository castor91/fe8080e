from instructions.Instruction import Instruction
class _9F(Instruction):
    def __init__(self):
        super(_9F, self).__init__('9F', 'SBB A', 1, 'Z, S, P, CY, AC')
    def function(self, state):
        # A <- A - A - CY

        raise NotImplementedError()
