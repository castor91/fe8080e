from instructions.Instruction import Instruction
class _BA(Instruction):
    def __init__(self):
        super(_BA, self).__init__('BA', 'CMP D', 1, 'Z, S, P, CY, AC')
    def function(self, state):
        # A - D

        raise NotImplementedError()
