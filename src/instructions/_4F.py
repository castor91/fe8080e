from instructions.Instruction import Instruction

class _4F(Instruction):

    def __init__(self):
        super(_4F, self).__init__('4F', 'MOV C,A', 1, None)

    def function(self, state):
        # C <- A
        raise NotImplementedError()
