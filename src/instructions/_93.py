from instructions.Instruction import Instruction
class _93(Instruction):
    def __init__(self):
        super(_93, self).__init__('93', 'SUB E', 1, 'Z, S, P, CY, AC')
    def function(self, state):
        # A <- A - E

        raise NotImplementedError()
