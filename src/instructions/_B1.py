from instructions.Instruction import Instruction
class _B1(Instruction):
    def __init__(self):
        super(_B1, self).__init__('B1', 'ORA C', 1, 'Z, S, P, CY, AC')
    def function(self, state):
        # A <- A | C

        raise NotImplementedError()
