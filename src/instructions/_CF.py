from instructions.Instruction import Instruction
class _CF(Instruction):
    def __init__(self):
        super(_CF, self).__init__('CF', 'RST 1', 1, 'None')
    def function(self, state):
        # CALL $8

        raise NotImplementedError()
