from instructions.Instruction import Instruction
class _77(Instruction):

    def __init__(self):
        super(_77, self).__init__('77', 'MOV M,A', 1, 'None')

    def function(self, state):
        # (HL) <- C
        adr = int('{:02x}{:02x}'.format(state.h, state.l), 16)
        state.memory[adr] = state.c
        state.pc += self.get_size()
