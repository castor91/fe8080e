from instructions.Instruction import Instruction
class _C5(Instruction):
    def __init__(self):
        super(_C5, self).__init__('C5', 'PUSH B', 1, 'None')
    def function(self, state):
        # (sp-2)<-C; (sp-1)<-B; sp <- sp - 2

        raise NotImplementedError()
