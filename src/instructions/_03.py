from instructions.Instruction import Instruction

class _03(Instruction):

    def __init__(self):
        super(_03, self).__init__('03', 'INX B', 1, None)

    def function(self, state):
        # BC <- BC + 1
        raise NotImplementedError()

