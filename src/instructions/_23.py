from instructions.Instruction import Instruction

class _23(Instruction):

    def __init__(self):
        super(_23, self).__init__('23', 'INX H', 1, None)

    def function(self, state):
        # HL <- HL + 1
        value = int('{:02x}{:02x}'.format(state.h, state.l), 16)
        value = (value + 1) % 0xFFFF
        state.h = Instruction.hi(value)
        state.l = Instruction.lo(value)
        state.pc += self.get_size()

