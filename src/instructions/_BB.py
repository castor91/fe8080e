from instructions.Instruction import Instruction
class _BB(Instruction):
    def __init__(self):
        super(_BB, self).__init__('BB', 'CMP E', 1, 'Z, S, P, CY, AC')
    def function(self, state):
        # A - E

        raise NotImplementedError()
