from instructions.Instruction import Instruction
class _F5(Instruction):
    def __init__(self):
        super(_F5, self).__init__('F5', 'PUSH PSW', 1, 'None')
    def function(self, state):
        # (sp-2)<-flags; (sp-1)<-A; sp <- sp - 2

        raise NotImplementedError()
