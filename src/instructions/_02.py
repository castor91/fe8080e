from instructions.Instruction import Instruction

class _02(Instruction):

    def __init__(self):
        super(_02, self).__init__('02', 'STAX B', 1, None)

    def function(self, state):
        # (BC) <- A
        raise NotImplementedError()

