from instructions.Instruction import Instruction

class _30(Instruction):

    def __init__(self):
        super(_30, self).__init__('30', 'SIM', 1, None)

    def function(self, state):
        # special
        raise NotImplementedError()
