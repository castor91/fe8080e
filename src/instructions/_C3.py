from instructions.Instruction import Instruction

class _C3(Instruction):

    def __init__(self):
        super(_C3, self).__init__('C3', 'JMP', 3, 'None')
        self._value = None

    def set_value(self, value):
        self._value = int.from_bytes(value, byteorder='little')

    def function(self, state):
        # PC <= adr
        state.pc = self._value

    def __str__(self):
        return '{} 0x{:04x}'.format(super(_C3, self).__str__(), self._value)

