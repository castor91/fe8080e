from instructions.Instruction import Instruction
class _9C(Instruction):
    def __init__(self):
        super(_9C, self).__init__('9C', 'SBB H', 1, 'Z, S, P, CY, AC')
    def function(self, state):
        # A <- A - H - CY

        raise NotImplementedError()
