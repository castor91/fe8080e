from instructions.Instruction import Instruction
class _7A(Instruction):
    def __init__(self):
        super(_7A, self).__init__('7A', 'MOV A,D', 1, 'None')
    def function(self, state):
        # A <- D

        raise NotImplementedError()
