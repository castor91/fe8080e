from instructions.Instruction import Instruction

class _50(Instruction):

    def __init__(self):
        super(_50, self).__init__('50', 'MOV D,B', 1, None)

    def function(self, state):
        # D <- B
        raise NotImplementedError()
