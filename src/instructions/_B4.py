from instructions.Instruction import Instruction
class _B4(Instruction):
    def __init__(self):
        super(_B4, self).__init__('B4', 'ORA H', 1, 'Z, S, P, CY, AC')
    def function(self, state):
        # A <- A | H

        raise NotImplementedError()
