from instructions.Instruction import Instruction

class _6E(Instruction):

    def __init__(self):
        super(_6E, self).__init__('6E', 'MOV L,M', 1, None)

    def function(self, state):
        # L <- (HL)
        raise NotImplementedError()
