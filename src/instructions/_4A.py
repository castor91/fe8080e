from instructions.Instruction import Instruction

class _4A(Instruction):

    def __init__(self):
        super(_4A, self).__init__('4A', 'MOV C,D', 1, None)

    def function(self, state):
        # C <- D
        raise NotImplementedError()
