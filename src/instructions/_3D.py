from instructions.Instruction import Instruction

class _3D(Instruction):

    def __init__(self):
        super(_3D, self).__init__('3D', 'DCR A', 1, 'Z, S, P, AC')

    def function(self, state):
        # A <- A - 1
        raise NotImplementedError()
