from instructions.Instruction import Instruction

class _1B(Instruction):

    def __init__(self):
        super(_1B, self).__init__('1B', 'DCX D', 1, None)

    def function(self, state):
        # DE = DE - 1
        raise NotImplementedError()
