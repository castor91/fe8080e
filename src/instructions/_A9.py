from instructions.Instruction import Instruction
class _A9(Instruction):
    def __init__(self):
        super(_A9, self).__init__('A9', 'XRA C', 1, 'Z, S, P, CY, AC')
    def function(self, state):
        # A <- A ^ C

        raise NotImplementedError()
